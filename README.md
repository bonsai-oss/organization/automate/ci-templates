# CI Templates

## Templates
| Template Path                                      | Description                    |
|----------------------------------------------------|--------------------------------|
| [`language/dotnet`](templates/language/dotnet.yml) | test dotnet projects           |
| [`language/go`](templates/language/go.yml)         | test go/golang projects        |
| [`release/semver`](templates/release/semver.yml)   | semantic versioning            |
| [`release/kaniko`](templates/release/kaniko.yml)   | kaniko based container release |

## How to use a template?
The template can be included via the following snippet:
```yaml
.<template name>_template_defaults:
  stage: <my_stage>
include:
  - project: bonsai-oss/organization/automate/ci-templates
    file: templates/<template path>.yml
    ref: main
```
Please note, that every template has the option to set parameters by inside the `<template name>_template_defaults` object.
Just copy the snippet above and replace the `<template path>` with the path of the template you want to use. **If your pipeline uses stages, you need to define a target stage for the template**.
